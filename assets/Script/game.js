cc.Class({
    extends: cc.Component,

    properties: {
        // 引导的手型
        hand: {
            type: cc.Node,
            default: null,
        },

        startAnswer: {
            type: cc.Node,
            default: null,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // 获取引导阶段
        const step = cc.sys.localStorage.getItem("guideStep") || "0";
        cc.log("step", step);
        if (step === "0") {
            this.hand.active = true;
            this.setHand(this.startAnswer.position);
            // 第一次引导, 先注册，否则 _touchListener 会报 null
            this.node.on(cc.Node.EventType.TOUCH_START, this.guideListener, this);
        }
    },

    start () {

    },
    // 开始答题
    startGame() {
        cc.director.loadScene('quiz');
    },

    guideListener(event) {
        let pos = this.node.parent.convertToNodeSpaceAR(event.getLocation());
        const rect = this.startAnswer.getBoundingBox();
        // 判断触摸点是否在按钮上
        if (rect.contains(pos)) {
            // 可以点击
            this.node._touchListener.setSwallowTouches(false);
            cc.log("enable touch");
            cc.sys.localStorage.setItem("guideStep", "1");
        } else {
            // 吞噬触摸，禁止触摸事件传递给按钮(禁止冒泡)
            this.node._touchListener.setSwallowTouches(true);
            cc.log("disable touch");
        }
    },

    setHand(pos) {
        // 引导手
        this.hand.setPosition(cc.v2(pos.x, pos.y - 70));
        const moveForward = cc.moveBy(0.9, cc.v2(0, 50));
        const moveBack = cc.moveBy(0.9, cc.v2(0, -50));
        const repeat = cc.repeatForever(cc.sequence(moveForward, moveBack));
        // 停止之前的动作
        this.hand.stopAllActions();
        this.hand.runAction(repeat);
    }


});
