const g = require("../utils/constants");

class CellModel {
    constructor() {
        // // 格子编号， 从1开始
        // 格子的像素位置
        this.posX = 0;
        this.posY = 0;
        // 格子显示的汉字
        this.name = "";
        // 需要输入的字
        this.input = "";
        // 运行动作的名字：action, playTime
        this.cmd = [];
        // 格子坐标
        this.x = 0;
        this.y = 0;
        // 填充格子在 selectCell 的坐标
        this.fillX = 0;
        this.fillY = 0;
    }

    // 设置位置
    setPos(posX, posY) {
        this.posX = posX;
        this.posY = posY;
    }

    // 更新选择的坐标
    setFillXY(x, y) {
        this.fillX = x;
        this.fillY = y;
    }

    // 重置
    reset() {
        this.name = "";
        this.input = "";
        this.cmd = [];
        this.fillX = 0;
        this.fillY = 0;
        this.posX = 0;
        this.posY = 0;
    }

    cleanCmd() {
        this.cmd = [];
    }

    // actions
    // 选中
    activeSelect() {
        this.cmd.push({
            action: g.Action.toggleActive,
        })
    }

    activeCorrectWord() {
        this.cmd.push({
            action: g.Action.changeBG,
            bgName: 'green',
        })
    }

    activeWrongWord() {
        this.cmd.push({
            action: g.Action.changeBG,
            bgName: 'red',
        })
    }

    activeNormalWord() {
        this.cmd.push({
            action: g.Action.changeBG,
            bgName: "gray",
        })
    }

    activeUpdateName(name) {
        this.name = name;
        this.cmd.push({
            action: g.Action.updateName,
        });
    }

    activeFadeOut() {
        this.cmd.push({
            action: g.Action.fadeOut,
            playTime: 0.6,
        });
    }

    activeWordFadIn() {
        this.cmd.push({
            action: g.Action.wordFadeIn,
            playTime: 0.5,
        });
    }

    activeMoveTo(posX, posY) {
        this.cmd.push({
            action: g.Action.moveTo,
            playTime: 0.6,
            posX,
            posY,
        });
    }

    activeDestroy() {
        this.cmd.push({
            action: g.Action.destroy,
        })
    }

    activeHide() {
        this.cmd.push({
            action: g.Action.hide,
        })
    }

    // 显示节点
    activeDisplay() {
        this.cmd.push({
            action: g.Action.active,
        })
    }



}

module.exports = {
    CellModel,
}