
cc.Class({
    extends: cc.Component,

    properties: {
        click: {
            type: cc.AudioClip,
            default: null,
        },
        // 选择
        selectWord: {
            type: cc.AudioClip,
            default: null,
        },
        incorrect: {
            type: cc.AudioClip,
            default: null,
        },
        correct: {
            type: cc.AudioClip,
            default: null,
        },
        finish: {
            type: cc.AudioClip,
            default: null,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},
    // TODO volume 需要读取配置

    start () {

    },

    // 播放点击效果
    playClick: function () {
        cc.audioEngine.play(this.click, false, 0.6);
    },

    playSelectWord: function () {
        cc.audioEngine.play(this.selectWord, false, 0.6);
    },

    playCorrect: function () {
        cc.audioEngine.play(this.correct, false, 0.6);
    },

    playIncorrect: function () {
        cc.audioEngine.play(this.incorrect, false, 0.6);
    },

    playFinish: function () {
        cc.audioEngine.play(this.finish, false, 0.6);
    }
});
