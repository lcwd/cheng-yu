const audio = require("./audio");
// 不需要手动添加 button 组件
cc.Class({
    extends: cc.Component,

    properties: {
        pressedScale: 0.6,
        transDuration: 0.2,
        // 声音, 可以没有
        audioUtils: {
            type: audio,
            default: null,
        }
    },

    // use this for initialization
    onLoad: function () {
        // 防止 this 指向 window
        const self = this;
        self.initScale = this.node.scale;
        self.button = self.getComponent(cc.Button);
        self.scaleDownAction = cc.scaleTo(self.transDuration, self.pressedScale);
        self.scaleUpAction = cc.scaleTo(self.transDuration, self.initScale);
        function onTouchDown (event) {
            this.stopAllActions();
            if(self.audioUtils) {
                self.audioUtils.playClick();
            }
            this.runAction(self.scaleDownAction);
        }
        function onTouchUp (event) {
            this.stopAllActions();
            this.runAction(self.scaleUpAction);
        }
        this.node.on(cc.Node.EventType.TOUCH_START, onTouchDown, this.node);
        this.node.on(cc.Node.EventType.TOUCH_END, onTouchUp, this.node);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, onTouchUp, this.node);
    }
});
