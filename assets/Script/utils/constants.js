// 成语行、列数
const PHASE_ROW = 7;
const PHASE_COL = 7;
// 选择区的行数
const SELECT_ROW = 3;
// 宽、高，用于摆放格子
const PHASE_WIDTH = 600;
const PHASE_HEIGHT = 525;
const SELECT_WIDTH = 600;
const SELECT_HEIGHT = 225;
const CELL_WIDTH = 70;
const CELL_HEIGHT = 70;
// cell 动作
const Action = {
    // 是否选中
    toggleActive: 1,
    // 更新格子名字
    updateName: 2,
    // 移动格子位置到目标位置
    moveTo: 3,
    // 消除
    destroy: 4,
    // 淡出
    fadeOut: 5,
    // 汉字渐显
    wordFadeIn: 6,
    // 隐藏节点
    hide: 7,
    // 激活节点
    active: 8,
    // 更新背景
    changeBG: 9,

}


module.exports = {
    PHASE_ROW,
    PHASE_COL,
    PHASE_WIDTH,
    PHASE_HEIGHT,
    CELL_WIDTH,
    CELL_HEIGHT,
    SELECT_ROW,
    SELECT_WIDTH,
    SELECT_HEIGHT,
    Action,
}