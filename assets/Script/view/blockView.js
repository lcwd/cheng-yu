// 生成格子
const g = require("../utils/constants");

cc.Class({
    extends: cc.Component,

    properties: {
        // 方块预制件
        cellPrefab: {
            type: cc.Prefab,
            default: null,
        },
        // 数值
        ox: {
            type: cc.Integer,
            default: 0,
        },
        oy: {
            type: cc.Integer,
            default: 0,
        },
        width: {
            type: cc.Integer,
            default: 0,
        },
        height: {
            type: cc.Integer,
            default: 0,
        },
        rows: {
            type: cc.Integer,
            default: 0,
        },
        isPlaying: {
            type: cc.Object,
            default: null,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    },

    start () {

    },

    initWithModel(cells, width, height, rows, cols) {
        const cellViews = [];
        this.isPlaying = false;
        // 偏移像素
        const ox = Math.floor((width - g.CELL_WIDTH*cols) / cols);
        const oy = Math.floor((height - g.CELL_HEIGHT*rows) / rows);
        this.ox = ox;
        this.oy = oy;
        this.width = width;
        this.height = height;
        this.rows = rows;
        for (let i=1; i <= rows; i++) {
            cellViews[i] = [];
            for (let j=1; j <= cols; j++){
                const cell = cc.instantiate(this.cellPrefab);
                this.node.addChild(cell);
                const cellView = cell.getComponent("cellView");
                cellView.initWithCellModel(cells[i][j], width, height, ox, oy, i, j);
                cellViews[i][j] = cellView;
            }
        }
        return cellViews;
    },

    onSuccess(cellPos) {
        cc.log(`touch position (${cellPos.x}, ${cellPos.y})`);
    },

    onFail() {
        cc.log('can not touch')
    },

    // 监听点击
    setListener: function (onSuccess, onFail) {
        this.node.on(cc.Node.EventType.TOUCH_END, function (eventTouch) {
            if(this.isPlaying) {
                // 正在播放
                return ;
            }
            const touchPos = eventTouch.getLocation();
            const cellPos = this.convertTouchPosToCell(touchPos);
            if(cellPos) {
                // 成功，调用回调
                cc.log(`touch (${cellPos.x}, ${cellPos.y})`);
                if (onSuccess) {
                    onSuccess(cellPos);
                } else {
                    this.onSuccess(cellPos);
                }
            } else {
                // 失败
                if(onFail) {
                    onFail();
                } else {
                    this.onFail();
                }
            }
        }, this)
    },

    // 根据点击的像素位置，转换成网格中的位置
    convertTouchPosToCell(pos) {
        // 将一个点转换到节点 (局部) 坐标系
        pos = this.node.convertToNodeSpaceAR(pos);
        // 判断边界
        if(pos.x < 0 || pos.x >= this.width || pos.y < 0 || pos.y >= this.height) {
            return false;
        }
        const y = Math.floor(pos.x / (this.ox + g.CELL_WIDTH)) + 1;
        const x = this.rows - Math.floor(pos.y / (this.oy + g.CELL_HEIGHT));

        return cc.v2(x, y);
    },

});
