const g = require("../utils/constants");
const audio = require("../utils/audio");

cc.Class({
    extends: cc.Component,

    properties: {
        word: {
            type: cc.Label,
            default: null,
        },
        // 被选中的格子
        selected: {
            type: cc.Sprite,
            default: null,
        },
        background: {
            type: cc.Sprite,
            default: null,
        },
        // 声音
        audioUtils: {
            type: audio,
            default: null,
        },
        // 待填充背景
        greySprite: {
            type: cc.SpriteFrame,
            default: null,
        },
        greenSprite: {
            type: cc.SpriteFrame,
            default: null,
        },
        redSprite: {
            type: cc.SpriteFrame,
            default: null,
        },

        // custom data property
        isSelected: {
            type: cc.Object,
            default: null,
        },
        // cell model
        m: {
            type: cc.Object,
            default: null,
        },
    },

    // onLoad() {
    //
    // },

    start () {

    },

    // update (dt) {},
    // 添加数据 model
    initWithCellModel(m, width, height, ox, oy, x, y) {
        this.m = m;
        // 计算像素位置
        const posX = (g.CELL_WIDTH+ ox)*(y - 0.5);
        const posY = height - (oy+ g.CELL_HEIGHT)*(x-0.5);
        this.node.x = posX;
        this.node.y = posY;
        this.m.setPos(posX, posY);
        this.word.string = this.m.name;
        // 显示需要操作的格子
        this.node.active = m.name !== "" || m.input !== "";
        if(m.input !== "" && this.node.active) {
            this.background.spriteFrame = this.greySprite;
        }
        this.isSelected = false;
        this.toggleBorder();
    },

    toggleBorder() {
        if (this.m.name !== "" && this.m.input === "") {
            // 不需要操作
            return ;
        }
        this.selected.node.active = this.isSelected;
    },

    // 选中本格
    toggleSelected: function () {
        this.isSelected = !this.isSelected;
        this.toggleBorder();
    },

    updateName() {
        this.word.string = this.m.name;
    },

    performAction() {
        // 移动的动作
        const actions = [];
        const wordActions = [];
        // let wordTotalTime
        let totalTime = 0;
        // let isClean = true;
        for (const cmd of this.m.cmd) {
            switch (cmd.action) {
                case g.Action.updateName:
                    this.updateName();
                    break;
                case g.Action.toggleActive:
                    this.toggleSelected();
                    this.audioUtils.playSelectWord();
                    break;
                case g.Action.moveTo:
                    // 动作太多，会出bug
                    totalTime += cmd.playTime;
                    actions.push(cc.moveTo(cmd.playTime, cc.v2(cmd.posX, cmd.posY)));
                    break;
                case g.Action.fadeOut:
                    totalTime += cmd.playTime;
                    actions.push(cc.fadeOut(cmd.playTime));
                    break;
                case g.Action.destroy:
                    const finishDie = cc.callFunc(function () {
                        this.node.destroy();
                    }, this)
                    actions.push(cc.delayTime(totalTime));
                    actions.push(finishDie);
                    break
                case g.Action.wordFadeIn:
                    // 透明度先设为 0 才用效
                    this.word.node.opacity = 0;
                    totalTime += cmd.playTime;
                    wordActions.push(cc.fadeIn(cmd.playTime));
                    break;
                case g.Action.hide:
                    this.node.active = false;
                    break;
                case g.Action.active:
                    this.node.active = true;
                    break;
                case g.Action.changeBG:
                    if(cmd.bgName === "green") {
                        this.background.spriteFrame = this.greenSprite;
                        this.audioUtils.playCorrect();
                    } else if(cmd.bgName === "red") {
                        this.background.spriteFrame = this.redSprite;
                        this.audioUtils.playIncorrect();
                    } else {
                        this.background.spriteFrame = this.greySprite;
                    }
                    break;
                default:
                    cc.log("unknown action", cmd.action);
            }
        }
        if (actions.length > 1) {
            this.node.runAction(cc.sequence(...actions));
        } else if(actions.length> 0) {
            this.node.runAction(actions[0]);
        }
        if (wordActions.length > 0) {
            this.word.node.runAction(wordActions[0]);
        }
        this.m.cleanCmd();
        return totalTime;
    },

});
