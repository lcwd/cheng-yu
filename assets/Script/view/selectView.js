const g = require("../utils/constants");

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {
    //     this.setLister();
    // },

    start () {

    },

    initWithModel(controller, gameModel) {
        this.gameModel = gameModel;
        const blockView = this.getComponent("blockView");
        if(this.cellViews) {
            for (let i=1; i <= g.SELECT_ROW; i++) {
                for (let j=1; j <= g.PHASE_COL; j++){
                    this.cellViews[i][j].node.destroy();
                }
            }
        }        this.cellViews = blockView.initWithModel(gameModel.selectCells, g.SELECT_WIDTH, g.SELECT_HEIGHT, g.SELECT_ROW, g.PHASE_COL);
        this.blockView = blockView;
        this.controller = controller;
        this.setListener();
    },

    // 监听点击
    setListener: function () {
        const onSuccess = function (cellPos) {
            if(this.cellViews[cellPos.x][cellPos.y].node.active) {
                // 当前格子可选
                this.gameModel.fillCell(cellPos.x, cellPos.y);
                this.controller.updateView();
            }
        }.bind(this);
        this.blockView.setListener(onSuccess);
    },

    updateView(cells) {
        for (const cell of cells) {
            this.cellViews[cell.x][cell.y].performAction();
        }
    },
    // 一段时间内禁止操作，不需要通过动画播放完后的动作回调
    disableTouch(dt) {
        if (dt <= 0) {
            return ;
        }
        this.blockView.isPlaying = true;
        this.blockView.node.runAction(cc.sequence(cc.delayTime(dt), cc.callFunc(function () {
            this.isPlaying = false;
            // this.audioUtils.playContinuousMatch(step);
        }, this.blockView)))
    },
});
